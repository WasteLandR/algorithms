import numpy as np

def QuickSort(arr, low, high):
    if high > low:
        p = partition(arr, low, high)
        QuickSort(arr, low, p - 1)
        QuickSort(arr, p + 1, high)
    return arr

def partition(arr, low, high):
    pivot = arr[high]
    i = low
    for j in range(low, high):
        if arr[j] < pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i += 1
    arr[i], arr[high] = arr[high], arr[i]
    return i


randmas = np.random.randint(100, size=100)
print(randmas)
#   mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
print(QuickSort(randmas, 0, len(randmas)-1))
