### Heap Sort
# Perfomance:
# Best: O(n*log(n))
# Worst: O(n*log(n))
# Memory O(1)

# Returning heap to the list
def Heapify(arr, size, root):
    left = 2 * root
    right = 2 * root + 1
    largest = root
    if left < size and arr[left] > arr[largest]:
        largest = left

    if right < size and arr[right] > arr[largest]:
        largest = right

    if largest != root:
        arr[root], arr[largest] = arr[largest], arr[root]
        Heapify(arr, size, largest)

# Transforming list to the MaxHeap 
def HeapSort(arr):
    n = len(arr)
    for i in range(n, -1, -1):
        Heapify(arr, n, i)

    for i in range(n - 1, 0, -1):
        arr[i], arr[0] = arr[0], arr[i]
        Heapify(arr, i, 0)
    return arr

mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
print(HeapSort(mas))