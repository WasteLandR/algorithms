### Selection Sort
# Unstable
# Best perfomace: O(n^2) (squared)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def SelectionSort(arr):
    n = len(arr)
    for i in range(n):
        minpos = i
        for j in range(i+1, n):
            if arr[minpos] > arr[j]:
                minpos = j
        arr[i], arr[minpos] = arr[minpos], arr[i]
    return arr

### Stable Selection Sort
# Stable
# Best perfomace: O(n^2) (squared)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def StableSelectionSort(arr):
    n = len(arr)
    for i in range(n):
        minpos = i
        for j in range(i+1, n):
            if arr[minpos] > arr[j]:
                minpos = j
        
        key = arr[minpos]
        while minpos > i:
            arr[minpos] = arr[minpos-1]
            minpos -= 1
        arr[i] = key
    return arr

mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
SelectionSort(mas)
#StableSelectionSort(mas)
print(mas)