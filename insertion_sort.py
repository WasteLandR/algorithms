### Insertion Sort
# Stable
# Perfomance: 
# Best: O(n^2) (squared)
# Worst: O(n^2) (squared)
# Memory: 1
def InsertionSort(arr):
    n = len(arr)
    for i in range(1, n):
        key = arr[i]
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key
    return arr

### My implementation of Insertion Sort
# The difference is in swapping in every step and not cascading elements over each other as it was in previous algorithm
# Perfomance: 
# Best: O(n^2) (squared)
# Worst: O(n^2) (squared)
# Memory: 1
def MyInsertionSort(arr):
    n = len(arr)
    for i in range(1, n):
        for j in range(i, 0, -1):
            if arr[j] < arr[j-1]:
                arr[j], arr[j-1] = arr[j-1], arr[j]
            else:
                break
    return arr

mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
InsertionSort(mas)
#MyInsertionSort(mas)
print(mas)