import math

def FibonacciSequence(n):
    arr = []
    arr.append(0)
    arr.append(1)
    for i in range(n-2):
        arr.append(arr[i]+arr[i+1])
    return arr

### Formula to find N element of Fibonacci Sequence
def FiboncacciElement(n):
    return int((((math.sqrt(5) + 1) / 2) ** n) / math.sqrt(5) + 0.5)


### Function to find every Fibonacci Sequence in every array
def IfFibbonaci(arr):
    elements = []
    seq = []
    cont = False
    n = len(arr)
    for i in range(n-2):
        if arr[i] + arr[i+1] == arr[i+2]:
            if cont == False:
                seq.append(i+1)
                seq.append(i+2)
                seq.append(i+3)
                cont = True
                continue
            seq.append(i+3)

        elif seq == []:
            continue
        else:
            elements.append(seq)
            cont = False
            seq = []
    if seq != []:
        elements.append(seq)
    return elements


print(FibonacciSequence(27))
print(FiboncacciElement(10))
arr = [0, 1, 2, 3, 5, 8, 34, 55, 89, 4181, 6765, 10946]
print(IfFibbonaci(arr))