### Linear search
# Perfomance:
# Best: O(n)
# Worst: O(n)
# Memory: 1
def LinearSearch(arr, obj):
    n = len(arr)
    operationsDone = 0
    for i in range(n):
        operationsDone += 1
        if (obj == arr[i]):
            print("Operations done: ", operationsDone)
            return i
    print("Not found!")
    return None

arr = [2, 8, 9, 19, 21, 28, 31, 31, 38, 40, 41, 47, 62, 66, 74, 88, 92, 92, 97, 97]
print("Linear search result: ", LinearSearch(arr, 92))