def StringReverse(obj):
    obj = list(obj)
    i = 0
    n = len(obj) - 1
    while n - i > i:
        obj[n - i], obj[i] = obj[i], obj[n - i]
        i += 1
    return ''.join(obj)
    
a = input()
print(StringReverse(a))