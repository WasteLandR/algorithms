### Sorting Algorithms
# https://habr.com/ru/post/188010/
# Code by WasteLandR

### Standart Bubble Sort 
# Stable
# Best perfomace: O(n) (linear)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def BubbleSort(arr):
    n = len(arr)
    for i in range(n-1):
        for j in range(n-i-1):
            if arr[j] > arr[j+1]:
                # Python swap (using tmp object)
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return(arr)

### Optimized Bubble Sort
# The difference is in checking that previous iteration had swap in it.
# Stable
# Best perfomace: O(n) (linear)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def OptimizedBubbleSort(arr):
    n = len(arr)
    flag = False
    for i in range(n):
        flag = False
        for j in range(n-i-1):
            if arr[j] > arr[j+1]:
                # Python swap (using tmp object)
                arr[j], arr[j+1] = arr[j+1], arr[j]
                flag = True

        if not flag:
            break

    return(arr)

### Selection Sort
# Unstable
# Best perfomace: O(n^2) (squared)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def SelectionSort(arr):
    n = len(arr)
    for i in range(n):
        minpos = i
        for j in range(i+1, n):
            if arr[minpos] > arr[j]:
                minpos = j
        arr[i], arr[minpos] = arr[minpos], arr[i]
    return arr

### Stable Selection Sort
# Stable
# Best perfomace: O(n^2) (squared)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def StableSelectionSort(arr):
    n = len(arr)
    for i in range(n):
        minpos = i
        for j in range(i+1, n):
            if arr[minpos] > arr[j]:
                minpos = j
        
        key = arr[minpos]
        while minpos > i:
            arr[minpos] = arr[minpos-1]
            minpos -= 1
        arr[i] = key
    return arr

### Some kind of sort
# The path is about switching elements increasing the gap between them
# Looks like ...
def MySort(arr):
    n = len(arr)
    for i in range(n):
        for j in range(i+1, n):
            if arr[i] > arr[j]:
                arr[i], arr[j] = arr[j], arr[i]
    return arr

### Insertion Sort
# Stable
# Perfomance: 
# Best: O(n^2) (squared)
# Worst: O(n^2) (squared)
# Memory: 1
def InsertionSort(arr):
    n = len(arr)
    for i in range(1, n):
        key = arr[i]
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key
    return arr

### My implementation of Insertion Sort
# The difference is in swapping in every step and not cascading elements over each other as it was in previous algorithm
# Perfomance: 
# Best: O(n^2) (squared)
# Worst: O(n^2) (squared)
# Memory: 1
def MyInsertionSort(arr):
    n = len(arr)
    for i in range(1, n):
        for j in range(i, 0, -1):
            if arr[j] < arr[j-1]:
                arr[j], arr[j-1] = arr[j-1], arr[j]
            else:
                break
    return arr

### Merge Sort
# Perfomance:
# Best: O(n*log(n))
# Worst: O(n*log(n))
# Memory O(n)

# Merging function.
# Takes two arrays and merging them
# Returns new array
def Merge(left, right):
    sorted_arr = []
    left_idx = right_idx = 0
    left_n, right_n = len(left), len(right)

    for _ in range(left_n + right_n):
        if (left_idx < left_n and right_idx < right_n):
            if (left[left_idx] <= right[right_idx]):
               sorted_arr.append(left[left_idx])
               left_idx += 1
            else:
                sorted_arr.append(right[right_idx])
                right_idx += 1
        elif (left_idx == left_n):
            sorted_arr.append(right[right_idx])
            right_idx += 1
        elif (right_idx == right_n):
            sorted_arr.append(left[left_idx])
            left_idx += 1
    
    return sorted_arr

# Sorting algorithm
# Takes array to sort and recursively spliting it
def MergeSort(arr):
    n = len(arr)

    if n <= 1:
        return arr
    
    mid = n // 2
    left = MergeSort(arr[:mid])
    right = MergeSort(arr[mid:])

    return Merge(left, right)

### Heap Sort
# Perfomance:
# Best: O(n*log(n))
# Worst: O(n*log(n))
# Memory O(1)

# Returning heap to the list
def Heapify(arr, size, root):
    left = 2 * root
    right = 2 * root + 1
    largest = root
    if left < size and arr[left] > arr[largest]:
        largest = left

    if right < size and arr[right] > arr[largest]:
        largest = right

    if largest != root:
        arr[root], arr[largest] = arr[largest], arr[root]
        Heapify(arr, size, largest)

# Transforming list to the MaxHeap 
def HeapSort(arr):
    n = len(arr)
    for i in range(n, -1, -1):
        Heapify(arr, n, i)

    for i in range(n - 1, 0, -1):
        arr[i], arr[0] = arr[0], arr[i]
        Heapify(arr, i, 0)
    return arr

### Linear search
# Perfomance:
# Best: O(n)
# Worst: O(n)
# Memory: 1
def LinearSearch(arr, obj):
    n = len(arr)
    operationsDone = 0
    for i in range(n):
        operationsDone += 1
        if (obj == arr[i]):
            print("Operations done: ", operationsDone)
            return i
    print("Not found!")
    return None

### Binary search
# Perfomance:
# Best: O(log(n))
# Worst: O(log(n))
# Memory: O(n)
def BinarySearch(arr, obj):
    operationsDone = 0
    low = 0
    high = len(arr) - 1
    while (low <= high):
        mid = (low + high) // 2
        if (arr[mid] > obj):
            operationsDone += 1
            high = mid - 1
        elif (arr[mid] < obj):
            operationsDone += 1
            low = mid + 1
        else:
            operationsDone += 1
            print("Operations done: ", operationsDone)
            return mid
    
    print("Not found!")
    return None

ar = [4, 5, 3, 2, 4, 1]
mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
arr = [2, 8, 9, 19, 21, 28, 31, 31, 38, 40, 41, 47, 62, 66, 74, 88, 92, 92, 97, 97]
#searchMas = list(range(100)) # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
#badmas = list(reversed(range(-5, 5)))

#BubbleSort(mas)
#OptimizedBubbleSort(mas)
#SelectionSort(mas)
#StableSelectionSort(ar)
#InsertionSort(mas)
#MyInsertionSort(mas)
#print("Linear: ", LinearSearch(arr, 92))
#print("Binary: ", BinarySearch(arr, 92))
print(MergeSort(mas))
#print(ar)

