# README #

This repository is all about me learning algorithms and data structures

### What algorithms are there? ###

* Bubble sort
* Insertion sort
* Selection sort
* Heap sort
* Merge sort
* Quick sort
* Linear search
* Binary search
* String reverse
* Hashing algorithm
* Fibonacci sequence
* Factorial algorithms (recursive and cyclic)
* Greedy algorithm

Stay tuned...

