### Binary search
# Perfomance:
# Best: O(log(n))
# Worst: O(log(n))
# Memory: O(n)
def BinarySearch(arr, obj):
    operationsDone = 0
    low = 0
    high = len(arr) - 1
    while (low <= high):
        mid = (low + high) // 2
        if (arr[mid] > obj):
            operationsDone += 1
            high = mid - 1
        elif (arr[mid] < obj):
            operationsDone += 1
            low = mid + 1
        else:
            operationsDone += 1
            print("Operations done: ", operationsDone)
            return mid
    
    print("Not found!")
    return None

arr = [2, 8, 9, 19, 21, 28, 31, 31, 38, 40, 41, 47, 62, 66, 74, 88, 92, 92, 97, 97]
print("Binary search result: ", BinarySearch(arr, 92))