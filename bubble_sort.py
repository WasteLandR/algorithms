### Standart Bubble Sort 
# Stable
# Best perfomace: O(n) (linear)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def BubbleSort(arr):
    n = len(arr)
    for i in range(n-1):
        for j in range(n-i-1):
            if arr[j] > arr[j+1]:
                # Python swap (using tmp object)
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return(arr)

### Optimized Bubble Sort
# The difference is in checking that previous iteration had swap in it.
# Stable
# Best perfomace: O(n) (linear)
# Worst perfomance: O(n^2) (squared)
# Memory: O(1)
def OptimizedBubbleSort(arr):
    n = len(arr)
    flag = False
    for i in range(n):
        flag = False
        for j in range(n-i-1):
            if arr[j] > arr[j+1]:
                # Python swap (using tmp object)
                arr[j], arr[j+1] = arr[j+1], arr[j]
                flag = True

        if not flag:
            break

    return(arr)


mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
#BubbleSort(mas)
OptimizedBubbleSort(mas)
print(mas)