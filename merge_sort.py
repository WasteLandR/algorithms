### Merge Sort
# Perfomance:
# Best: O(n*log(n))
# Worst: O(n*log(n))
# Memory O(n)

# Merging function.
# Takes two arrays and merging them
# Returns new array
def Merge(left, right):
    sorted_arr = []
    left_idx = right_idx = 0
    left_n, right_n = len(left), len(right)

    for _ in range(left_n + right_n):
        if (left_idx < left_n and right_idx < right_n):
            if (left[left_idx] <= right[right_idx]):
               sorted_arr.append(left[left_idx])
               left_idx += 1
            else:
                sorted_arr.append(right[right_idx])
                right_idx += 1
        elif (left_idx == left_n):
            sorted_arr.append(right[right_idx])
            right_idx += 1
        elif (right_idx == right_n):
            sorted_arr.append(left[left_idx])
            left_idx += 1
    
    return sorted_arr

# Sorting algorithm
# Takes array to sort and recursively spliting it
def MergeSort(arr):
    n = len(arr)

    if n <= 1:
        return arr
    
    mid = n // 2
    left = MergeSort(arr[:mid])
    right = MergeSort(arr[mid:])

    return Merge(left, right)

mas = [3, 2, 4, 5, 12, 11, 6, 9, 8, 1, 7, 10, 1]
print(MergeSort(mas))